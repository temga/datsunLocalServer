import express from 'express'
import http from 'http'
import api from './api/main.js'
import bodyParser from 'body-parser'

let app = express();

app.use(bodyParser.urlencoded(
        {
            extended: true
        }
    )
);

app.use(
    bodyParser.json({
        limit: '50mb'
    })
);

const port = 3000;
const server = require('http').createServer(app);
app.use(api());
server.listen(port, (error) => {
    if (error) {
        console.error(error);
    } else {
        console.info("==> 🌎  Listening on port %s.", port);
        require("./db/db.js");
    }
});