var firstline = require('firstline');

export var config = {};

    firstline(`${__dirname}/city`).then((x) => {
    config = {
        city: x.split(' ')[0].replace('_',' '),
        region: x.split(' ')[1].replace('_',' '),
        id_date: x.split(' ')[2]
    };
})
    .catch((e) => console.log(e));