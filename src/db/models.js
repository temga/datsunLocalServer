import mongoose from 'mongoose'

const UserSchema = mongoose.Schema(
    {
        qr_id: {
            type: String,
            unique: true
        },
        f_name: {
            type: String,
            required: true
        },
        s_name: {
            type: String,
            required: true
        },
        p_name: {
            type: String,
            default: ""
        },
        bd: {
            type: String,
            default: ""
        },
        sex: {
            type: String,
            required: true
        },
        mail: {
            type: String,
            required: false
        },
        city: {
            type: String,
            required: true
        },
        car: {
            type: Number,
            required: true
        },
        phone: {
          type: String,
          required: true
        },
        song: {
            type: Number,
            default: 0
        },
        spam: {
            type: Boolean,
            default: false
        },
        inst0: {
            type: Boolean,
            default: false
        },
        inst1: {
            type: Number,
            default: 0
        },
        id_date: {
            type: String,
            default : "inty"
        }
    }
);

export let UserModel = mongoose.model('User', UserSchema);