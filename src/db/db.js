import mongoose from 'mongoose'

mongoose.connect('mongodb://localhost/dutsun_local');

let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

db.once('open', (cb) => {
    console.log("==> Connected to db...");
});
