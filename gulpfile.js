const gulp = require('gulp');
const babel = require('gulp-babel');
const concat = require('gulp-concat');

gulp.task('default', ['babel', 'copy'] );

gulp.task('copy', () => {
    return gulp.src(['./src/*fonts/*', './src/*photos/*'])
    .pipe(gulp.dest('bin/api'));
});

gulp.task('babel', () => {
    return gulp.src(['src/**/*.js', 'src/*.js'])
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(gulp.dest('bin'));
});