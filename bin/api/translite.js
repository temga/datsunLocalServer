#!/usr/bin/env node
'use strict';

/**
 @name      translit.js
 @author    XGuest <xguest@list.ru>
 @link      https://github.com/xguest/iso_9_js
 @version   1.0.1.0
 @copyright GPL applies.
 No warranties XGuest[11.02.2015/03:44:01] translit [ver.1.0.1.0]
 #guid      {E7088033-479F-47EF-A573-BBF3520F493C}

 @description Прямая и обратная транслитерация
 Соответствует ISO 9:1995 и ГОСТ 7.79-2000 системы А и Б

 @param {String}  str транслитерируемая строка
 @param {Number}  typ ± направление (тип) транслитерации
 + прямая с латиницы в кирилицу
 - обратная
 system A = 1-диакритика;
 system B = 2-Беларусь;3-Болгария;4-Македония;5-Россия;6-Украина;
 @example
 function example() {
  var a, b = [
     [],
     ["Диакритика", "Съешь ещё этих мягких французских булок, да выпей же чаю!"],
     ["Беларускую", "З'ясі яшчэ гэтых мяккіх французскіх булак, ды выпі ж чаю!"],
     ["Български",  "Яжте повече от тези меки кифлички, но също така се пие чай!"],
     ["Македонски", "Јадат повеќе од овие меки францускиот ролни, па пијат чај!"],
     ["Русский",    "Съешь ещё этих мягких французских булок, да выпей же чаю!"],
     ["Українська", "З'їж ще цих м'яких французьких булок, та випий же чаю!"]
  ], c, d;
  for(a = 1; a < b.length - 1; a++) {
   c = b[a][0];                                       // Language
   d = b[a][1];                                       // Source
   e = translit(d, a);                                // Translit
   console.log(
    "%s - %s\nSource  : %s\nTranslit: %s\nReverse : %s\n",
    c,                                                // Language
    translit(c, a),                                   // Transliterated language
    d,                                                // Source
    e,                                                // Translit
    translit(e, -1 * a)                               // Reverse
   );
  }
 };
 */
function translit(str, typ) {
    var func = function (typ) {
        /* Function Expression
         * Вспомогательная функция.
         *
         * В ней и хотелось навести порядок.
         *
         * Проверяет направление транслитерации.
         * Пред-обработка строки (правила из ГОСТ).
         * Возвращает массив из 2 функций:
         *  построения таблиц транслитерации.
         *  и пост-обработки строки (правила из ГОСТ).
         *
         * @param  {Number} typ
         * @return {Array}  Массив функций пред и пост обработки.
         */
        var abs = Math.abs(typ); // Абсолютное значение транслитерации
        if (typ === abs) {
            // Прямая транслитерация(кириллица в латиницу)
            // Правила транслитерации (из ГОСТ).
            // "i`" только перед согласными в ст. рус. и болг.
            //  str = str.replace(/(i(?=.[^аеиоуъ\s]+))/ig, "1`");
            str = str.replace(/(\u0456(?=.[^\u0430\u0435\u0438\u043E\u0443\u044A\s]+))/ig, '$1`');
            return [// Возвращаем массив функций
            function (col, row) {
                // создаем таблицу и RegExp
                var chr; // Символ
                if (chr = col[0] || col[abs]) {
                    // Если символ есть
                    trantab[row] = chr; // Добавляем символ в объект преобразования
                    regarr.push(row); // Добавляем в массив RegExp
                }
            },
            // функция пост-обработки
            function (str) {
                // str - транслируемая строка.
                // Правила транслитерации (из ГОСТ).
                return str.replace(/i``/ig, 'i`'). // "i`" только перед согласными в ст. рус. и болг.
                replace(/((c)z)(?=[ieyj])/ig, '$2'); // "cz" в символ "c"
            }];
        } else {
                // Обратная транслитерация (латиница в кириллицу)
                str = str.replace(/(c)(?=[ieyj])/ig, '$1z'); // Правило сочетания "cz"
                return [// Возвращаем массив функций
                function (col, row) {
                    // Создаем таблицу и RegExp
                    var chr; // Символа
                    if (chr = col[0] || col[abs]) {
                        // Если символ есть
                        trantab[chr] = row; // Добавляем символ в объект преобразования
                        regarr.push(chr); // Добавляем в массив RegExp
                    }
                },
                // функция пост-обработки
                function (str) {
                    return str;
                }]; // nop - пустая функция.
            }
    }(typ);
    var iso9 = { // Объект описания стандарта
        // Имя - кириллица
        //   0 - общие для всех
        //   1 - диакритика         4 - MK|MKD - Македония
        //   2 - BY|BLR - Беларусь  5 - RU|RUS - Россия
        //   3 - BG|BGR - Болгария  6 - UA|UKR - Украина
        /*-Имя---------0-,------1--,---2-,---3-,---4-,----5-,---6-*/
        'щ': ["", 'ŝ', "", "sth", "", "shh", "shh"], // "щ"
        'я': ["", 'â', "ya", "ya", "", "ya", "ya"], // "я"
        'є': ["", 'ê', "", "", "", "", "ye"], // "є"
        'ѣ': ["", 'ě', "", "ye", "", "ye", ""], //  ять
        'і': ["", 'ì', "i", "i`", "", "i`", "i"], // "і" йота
        'ї': ["", 'ï', "", "", "", "", "yi"], // "ї"
        'ё': ["", 'ë', "yo", "", "", "yo", ""], // "ё"
        'ю': ["", 'û', "yu", "yu", "", "yu", "yu"], // "ю"
        'ж': ["zh", 'ž'], // "ж"
        'ч': ["ch", 'č'], // "ч"
        'ш': ["sh", 'š'], // "ш"
        'ѳ': ["", 'f̀', "", "fh", "", "fh", ""], //  фита
        'џ': ["", 'd̂', "", "", "dh", "", ""], // "џ"
        'ґ': ["", 'g̀', "", "", "", "", "g`"], // "ґ"
        'ѓ': ["", 'ǵ', "", "", "g`", "", ""], // "ѓ"
        'ѕ': ["", 'ẑ', "", "", "z`", "", ""], // "ѕ"
        'ќ': ["", 'ḱ', "", "", "k`", "", ""], // "ќ"
        'љ': ["", 'l̂', "", "", "l`", "", ""], // "љ"
        'њ': ["", 'n̂', "", "", "n`", "", ""], // "њ"
        'э': ["", 'è', "e`", "", "", "e`", ""], // "э"
        'ъ': ["", 'ʺ', "", "a`", "", "``", ""], // "ъ"
        'ы': ["", "y", "y`", "", "", "y`", ""], // "ы"
        'ў': ["", 'ǔ', "u`", "", "", "", ""], // "ў"
        'ѫ': ["", 'ǎ', "", "o`", "", "", ""], //  юс
        'ѵ': ["", 'ỳ', "", "yh", "", "yh", ""], //  ижица
        'ц': ["cz", "c"], // "ц"
        'а': ["a"], // "а"
        'б': ["b"], // "б"
        'в': ["v"], // "в"
        'г': ["g"], // "г"
        'д': ["d"], // "д"
        'е': ["e"], // "е"
        'з': ["z"], // "з"
        'и': ["", "i", "", "i", "i", "i", "y`"], // "и"
        'й': ["", "j", "j", "j", "", "j", "j"], // "й"
        'к': ["k"], // "к"
        'л': ["l"], // "л"
        'м': ["m"], // "м"
        'н': ["n"], // "н"
        'о': ["o"], // "о"
        'п': ["p"], // "п"
        'р': ["r"], // "р"
        'с': ["s"], // "с"
        'т': ["t"], // "т"
        'у': ["u"], // "у"
        'ф': ["f"], // "ф"
        'х': ["x", "h"], // "х"
        'ь': ["", 'ʹ', "`", "`", "", "`", "`"], // "ь"
        'ј': ["", 'ǰ', "", "", "j", "", ""], // "ј"
        '’': ["'", 'ʼ'], // "’"
        '№': ["#"] // "№"
        /*-Имя---------0-,------1--,---2-,---3-,---4-,----5-,---6-*/
    },
        regarr = [],
        trantab = {};
    for (var row in iso9) {
        func[0](iso9[row], row);
    } // Создание таблицы и массива RegExp
    return func[1]( // функция пост-обработки строки (правила и т.д.)
    str.replace( // Транслитерация
    new RegExp(regarr.join('|'), 'gi'), // Создаем RegExp из массива
    function (R) {
        // CallBack Функция RegExp
        if ( // Обработка строки с учетом регистра
        R.toLowerCase() === R) {
            return trantab[R];
        } else {
            return trantab[R.toLowerCase()].toUpperCase();
        }
    }));
}
module.exports = translit;