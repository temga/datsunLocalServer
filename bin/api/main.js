'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _express = require('express');

var _models = require('../db/models.js');

var _multer = require('multer');

var _multer2 = _interopRequireDefault(_multer);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _config = require('../config.js');

var _translite = require('./translite.js');

var _translite2 = _interopRequireDefault(_translite);

var _xlsx = require('xlsx');

var _xlsx2 = _interopRequireDefault(_xlsx);

var _pdfkit = require('pdfkit');

var _pdfkit2 = _interopRequireDefault(_pdfkit);

var _city = require('../reports/city.js');

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var exec = require('child_process').exec;

var ws_name = "DatsunRoadShow";

require('isomorphic-fetch');

exports.default = function () {
    var api = (0, _express.Router)();

    var storage = _multer2.default.diskStorage({
        filename: function filename(req, file, cb) {
            cb(null, file.originalname);
        },
        destination: function destination(req, file, cb) {
            cb(null, 'photos/');
        }
    });

    var upload = (0, _multer2.default)({
        dest: 'photos/',
        storage: storage
    });

    api.post("/user", function (req, res) {
        var new_user = new _models.UserModel(req.body);
        new_user.id_date = _config.config.id_date;
        new_user.save(function (err) {
            if (err) {
                console.log(err);
                return res.status(502).json({ err: err.code });
            }
            res.send("User added");
        });
    });

    api.get("/inst0", function (req, res) {
        _models.UserModel.findOne({ 'qr_id': req.query.qr_id }, 'inst0 phone song f_name', function (err, doc) {
            if (err) return res.status(502).json(err.code);
            if (doc) {
                if (+doc.inst0) return res.status(403).json({ err: 10 });
                res.json(doc);
                doc.inst0 = true;
                doc.save();
            } else {
                res.status(404).json({ err: 11 });
            }
        });
    });

    api.get("/inst1", function (req, res) {
        _models.UserModel.findOne({ 'qr_id': req.query.qr_id }, 'inst1 phone f_name', function (err, doc) {
            if (err) return res.status(502).json(err.code);
            if (doc) {
                if (doc.inst1 > 2) return res.status(403).json(doc);
                doc.city = _config.config.city;
                res.send(doc);
                doc.inst1++;
                doc.save();
            } else {
                res.status(404).json({ err: 11 });
            }
        });
    });

    api.post("/photo", upload.single("datsun_photo"), function (req, res) {
        res.send(req.file.path);
    });

    api.get("/report/:id_date", function (req, res) {
        _models.UserModel.find({ id_date: req.params.id_date }, function (err, docs) {
            if (err) return res.status(502).json(err.code);
            if (docs) {
                var data = [["Фамилия", "Имя", "Отчество", "Дата рождения", "Пол", "E-mail", "Телефон", "Город", "Регион", "Песня", "Караоке", "Фото", "Предпочитаемый автомобиль", "Рассылка"]];
                var _iteratorNormalCompletion = true;
                var _didIteratorError = false;
                var _iteratorError = undefined;

                try {
                    for (var _iterator = docs[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                        var doc = _step.value;

                        data.push([doc.s_name, doc.f_name, doc.p_name, doc.bd, doc.sex, doc.mail, doc.phone, doc.city, _config.config.region, doc.song, doc.inst0 ? "Да" : "Нет", doc.inst1, formatCar(doc.car), doc.spam ? "Да" : "Нет"]);
                    }
                } catch (err) {
                    _didIteratorError = true;
                    _iteratorError = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion && _iterator.return) {
                            _iterator.return();
                        }
                    } finally {
                        if (_didIteratorError) {
                            throw _iteratorError;
                        }
                    }
                }

                var wscols = [{ wch: 20 }, { wch: 20 }, { wch: 20 }, { wch: 20 }, { wch: 20 }, { wch: 20 }, { wch: 20 }, { wch: 20 }, { wch: 20 }];

                var wb = new Workbook();
                var ws = sheet_from_array_of_arrays(data);

                /* TEST: add worksheet to workbook */
                wb.SheetNames.push(ws_name);
                wb.Sheets[ws_name] = ws;

                /* TEST: column widths */
                ws['!cols'] = wscols;

                var date = new Date();
                var f_name = './reports/' + (0, _translite2.default)(_config.config.city, 1) + '_' + date.getHours() + ':' + date.getMinutes() + '.xlsx';

                var wopts = { bookType: 'xlsx', bookSST: false, type: 'buffer' };
                var buffer = _xlsx2.default.write(wb, wopts);
                res.setHeader('Content-Length', buffer.length);
                res.setHeader('Content-Type', 'application/vnd.ms-excel');
                res.setHeader('Content-Disposition', 'attachment; filename=' + f_name);
                res.write(buffer, f_name);
                res.end();
            } else {
                return res.status(404).send("Данные отсутствуют или неправильно задан город");
            }
        });
    });

    api.get('/city', function (req, res) {
        res.send(_config.config.city);
    });

    api.get('/qrcheck', function (req, res) {
        _models.UserModel.find({ qr_id: req.query.qr_id }, 'qr_id', function (err, docs) {
            if (docs.length > 0) return res.send("bad");
            res.send("good");
        });
    });

    api.post('/print', function (req, res) {
        _models.UserModel.find({ phone: req.body.phone }, 'phone', function (err, docs) {
            if (docs.length > 0) {
                return res.status(401).send("bad");
            }

            var pdf = new _pdfkit2.default({
                size: 'A5'
            });

            pdf.pipe(_fs2.default.createWriteStream(__dirname + '/output.pdf'));

            pdf.font(__dirname + '/fonts/Datsun-Bold.ttf');

            pdf.image(__dirname + '/photos/anketa.png', 0, 0, {
                width: pdf.page.width,
                height: pdf.page.height
            });

            pdf.text(req.body.f_name, 88, 75);
            pdf.text(req.body.p_name, 105, 100);
            pdf.text(req.body.s_name, 105, 125);

            if (req.body.sex == 'Мужчина') {

                pdf.image(__dirname + '/photos/ok.png', 92, 152, {
                    width: 15,
                    height: 15
                });
            } else {
                pdf.image(__dirname + '/photos/ok.png', 159, 152, {
                    width: 15,
                    height: 15
                });
            }

            pdf.text(req.body.bd.substring(0, 2), 168, 174);
            pdf.text(req.body.bd.substring(3, 5), 253, 174);
            pdf.text(req.body.bd.substring(6, 10), 331, 174, {
                lineBreak: false
            });

            pdf.text(req.body.mail, 87, 195);
            pdf.text(req.body.phone, 102, 220);
            pdf.text(_config.config.city, 87, 245);

            if (req.body.car == 1 || req.body.car == 3) pdf.image(__dirname + '/photos/ok.png', 92, 293, {
                width: 15,
                height: 15
            });

            if (req.body.car == 2 || req.body.car == 3) pdf.image(__dirname + '/photos/ok.png', 227, 293, {
                width: 15,
                height: 15
            });

            /*if (req.body.spam)
            {
                pdf.image(`${__dirname}/photos/ok.png`, 49, 511, {
                    width:15,
                    height:15
                });
            }
            else
            {
                pdf.image(`${__dirname}/photos/ok.png`, 166, 511, {
                    width:15,
                    height:15
                });
            }*/

            pdf.end();

            setTimeout(function () {
                exec('lp -d HP_LaserJet_CP_1025nw -o number-up=1 -o media=A5 -o fit-to-page -o scaling=100 -o page-top=0 -o page-right=0 -o page-left=0 ' + __dirname + '/output.pdf', function (err, stdout, stderr) {
                    if (err) {
                        console.error(err);
                        return;
                    }
                    console.log(stdout);
                    console.log(stderr);
                });
                res.send('ok');
            }, 1000);
        });
    });

    api.get('/qrsong', function (req, res) {
        _models.UserModel.findOne({ qr_id: req.query.qr_id }, 'qr_id song f_name s_name phone', function (err, docs) {
            if (!docs) return res.status(404).send("bad");
            res.json(docs);
        });
    });

    api.get("/songclear", function (req, res) {
        _models.UserModel.findOne({ qr_id: req.query.qr_id }, function (err, docs) {
            if (!docs) return res.status(404).send("bad");
            docs.inst0 = false;
            docs.save(function (err) {
                if (err) return res.status(502).send("bad");
                res.send("Ok");
            });
        });
    });

    api.get('/songupdate', function (req, res) {
        _models.UserModel.findOne({ qr_id: req.query.qr_id }, function (err, docs) {
            if (!docs) return res.status(404).send("bad");
            docs.song = req.query.song;
            docs.save(function (err) {
                if (err) return res.status(502).send("bad");
                res.send("Ok");
            });
        });
    });

    return api;
};

function formatCar(id) {
    switch (id) {
        case 0:
            return "Нет";break;
        case 1:
            return "DATSUN on-DO";break;
        case 2:
            return "DATSUN mi-DO";break;
        case 3:
            return "Оба автомобиля";break;
    }
}

/* dummy workbook constructor */
function Workbook() {
    if (!(this instanceof Workbook)) return new Workbook();
    this.SheetNames = [];
    this.Sheets = {};
}

/* convert an array of arrays in JS to a CSF spreadsheet */
function sheet_from_array_of_arrays(data, opts) {
    var ws = {};
    var range = { s: { c: 10000000, r: 10000000 }, e: { c: 0, r: 0 } };
    for (var R = 0; R != data.length; ++R) {
        for (var C = 0; C != data[R].length; ++C) {
            if (range.s.r > R) range.s.r = R;
            if (range.s.c > C) range.s.c = C;
            if (range.e.r < R) range.e.r = R;
            if (range.e.c < C) range.e.c = C;
            var cell = { v: data[R][C] };
            if (cell.v == null) continue;
            var cell_ref = _xlsx2.default.utils.encode_cell({ c: C, r: R });

            /* TEST: proper cell types and value handling */
            if (typeof cell.v === 'number') cell.t = 'n';else if (typeof cell.v === 'boolean') cell.t = 'b';else if (cell.v instanceof Date) {
                cell.t = 'n';cell.z = _xlsx2.default.SSF._table[14];
                cell.v = datenum(cell.v);
            } else cell.t = 's';
            ws[cell_ref] = cell;
        }
    }

    /* TEST: proper range */
    if (range.s.c < 10000000) ws['!ref'] = _xlsx2.default.utils.encode_range(range);
    return ws;
}

/* TODO: date1904 logic */
function datenum(v, date1904) {
    if (date1904) v += 1462;
    var epoch = Date.parse(v);
    return (epoch - new Date(Date.UTC(1899, 11, 30))) / (24 * 60 * 60 * 1000);
}

function formatRegion(vcity) {
    return _city.region['' + _lodash2.default.findKey(_city.city, function (o) {
        return _lodash2.default.indexOf(o, vcity) >= 0;
    })];
}