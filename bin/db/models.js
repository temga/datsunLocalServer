"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.UserModel = undefined;

var _mongoose = require("mongoose");

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var UserSchema = _mongoose2.default.Schema({
    qr_id: {
        type: String,
        unique: true
    },
    f_name: {
        type: String,
        required: true
    },
    s_name: {
        type: String,
        required: true
    },
    p_name: {
        type: String,
        default: ""
    },
    bd: {
        type: String,
        default: ""
    },
    sex: {
        type: String,
        required: true
    },
    mail: {
        type: String,
        required: false
    },
    city: {
        type: String,
        required: true
    },
    car: {
        type: Number,
        required: true
    },
    phone: {
        type: String,
        required: true
    },
    song: {
        type: Number,
        default: 0
    },
    spam: {
        type: Boolean,
        default: false
    },
    inst0: {
        type: Boolean,
        default: false
    },
    inst1: {
        type: Number,
        default: 0
    },
    id_date: {
        type: String,
        default: "inty"
    }
});

var UserModel = exports.UserModel = _mongoose2.default.model('User', UserSchema);