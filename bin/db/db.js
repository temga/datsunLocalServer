'use strict';

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_mongoose2.default.connect('mongodb://localhost/dutsun_local');

var db = _mongoose2.default.connection;
db.on('error', console.error.bind(console, 'connection error:'));

db.once('open', function (cb) {
    console.log("==> Connected to db...");
});