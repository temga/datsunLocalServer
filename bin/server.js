'use strict';

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _http = require('http');

var _http2 = _interopRequireDefault(_http);

var _main = require('./api/main.js');

var _main2 = _interopRequireDefault(_main);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var app = (0, _express2.default)();

app.use(_bodyParser2.default.urlencoded({
    extended: true
}));

app.use(_bodyParser2.default.json({
    limit: '50mb'
}));

var port = 3000;
var server = require('http').createServer(app);
app.use((0, _main2.default)());
server.listen(port, function (error) {
    if (error) {
        console.error(error);
    } else {
        console.info("==> 🌎  Listening on port %s.", port);
        require("./db/db.js");
    }
});