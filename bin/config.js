'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
var firstline = require('firstline');

var config = exports.config = {};

firstline(__dirname + '/city').then(function (x) {
    exports.config = config = {
        city: x.split(' ')[0].replace('_', ' '),
        region: x.split(' ')[1].replace('_', ' '),
        id_date: x.split(' ')[2]
    };
}).catch(function (e) {
    return console.log(e);
});